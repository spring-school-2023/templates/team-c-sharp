using NUnit.Framework;
using FluentAssertions;

namespace festo.springschool.model {

    [TestFixture]
    class HelloWorldTest {

        private HelloWorld _underTest;

        [SetUp]
        public void SetUp() {
            _underTest = new HelloWorld();
        }

        [Test]
        public void Test_MessageIsHelloWorld() {
            // Default assertions
            Assert.AreEqual("Hello World!", _underTest.Message);

            // FluentAssertions
            _underTest.Message.Should().Be("Hello World!");
        }

    }

}

