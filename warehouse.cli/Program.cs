﻿using System;

using festo.springschool.model;

namespace festo.springschool {

    class Program {

        public static void Main(string[] args) {
            var helloWorld = new HelloWorld();
            Console.WriteLine(helloWorld.Message);
        }

    }

}
